package com.example.elizabeth.djumediaplayer

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.media.MediaPlayer
import android.app.Activity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.mtechviral.mplaylib.MusicFinder
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.*

class MainActivity : Activity() {

    var Art: ImageView? = null

    var play: ImageButton? = null
    var shuffle: ImageButton? = null

    var Title: TextView? = null
    var Artist: TextView? = null

    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            //Ask for permission
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 0)
        } else {
            Player()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Player()
        } else {
            longToast("Permission not granted. Shutting down.")
            finish()
        }
    }

    private fun Player() {

        var songs = async {
            val songFinder = MusicFinder(contentResolver)
            songFinder.prepare()
            songFinder.allSongs
        }

        launch(kotlinx.coroutines.experimental.android.UI) {
            val songs = songs.await()

            val playerUI = object : AnkoComponent<MainActivity> {
                override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {

                    relativeLayout {
                        backgroundColor = Color.BLACK

                        Art = imageView {
                            scaleType = ImageView.ScaleType.FIT_CENTER
                        }.lparams(matchParent, matchParent)

                        verticalLayout {
                            backgroundColor = Color.parseColor("#99000000")
                            Title = textView {
                                textColor = Color.WHITE
                                typeface = Typeface.DEFAULT_BOLD
                                textSize = 18f
                            }

                            Artist = textView {
                                textColor = Color.WHITE
                            }

                            linearLayout {
                                play  = imageButton {
                                    imageResource = R.drawable.ic_play_arrow_black_24dp
                                    onClick {
                                        playPause()
                                    }
                                }.lparams(0, wrapContent, 0.5f)

                                shuffle = imageButton {
                                    imageResource = R.drawable.ic_shuffle_black_24dp
                                    onClick {
                                        Random()
                                    }
                                }.lparams(0, wrapContent, 0.5f)
                            }.lparams(matchParent, wrapContent) {
                                topMargin = dip(5)
                            }


                        }.lparams(matchParent, wrapContent) {
                            alignParentBottom()
                        }
                    }

                }

                fun Random() {
                    Collections.shuffle(songs)
                    val song = songs[0]
                    mediaPlayer?.reset()
                    mediaPlayer = MediaPlayer.create(ctx, song.uri)
                    mediaPlayer?.setOnCompletionListener {
                        Random()
                    }
                    Art?.imageURI = song.albumArt
                    Title?.text = song.title
                    Artist?.text = song.artist
                    mediaPlayer?.start()
                    play?.imageResource = R.drawable.ic_pause_black_24dp
                }

                fun playPause() {
                    var songPlaying: Boolean? = mediaPlayer?.isPlaying

                    if (songPlaying == true) {
                        mediaPlayer?.pause()
                        play?.imageResource = R.drawable.ic_play_arrow_black_24dp
                    } else {
                        mediaPlayer?.start()
                        play?.imageResource = R.drawable.ic_pause_black_24dp
                    }
                }
            }
            playerUI.setContentView(this@MainActivity)
            playerUI.Random()


        }
    }

    override fun onDestroy() {
        mediaPlayer?.release()
        super.onDestroy()
    }
}
